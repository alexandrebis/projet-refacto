using System;

namespace ESGI.DesignPattern.Projet
{
    public abstract class LoanFactory
    {
        public static Loan create(CapitalStrategyType capitalStrategyType, double commitment, DateTime start, DateTime? expiry, DateTime? maturity, int riskRating, int outStandingRiskAmount)
        {
            switch(capitalStrategyType)
            {
                case CapitalStrategyType.Term:
                    return new Loan(commitment, start, null,
                            maturity, riskRating, outStandingRiskAmount, new CapitalStrategyTermLoan());
                case CapitalStrategyType.Revolver:
                    return new Loan(commitment, start, expiry,
                            null, riskRating, outStandingRiskAmount, new CapitalStrategyRevolver());
                case CapitalStrategyType.AdvisedLine:
                    if (riskRating > 3) return null;
                    Loan advisedLine = new Loan(commitment, start, expiry,
                                                null, riskRating, outStandingRiskAmount, new CapitalStrategyAdvisedLine());
                    advisedLine.SetUnusedPercentage(0.1);
                    return advisedLine;
                default:
                    throw new System.Exception();
            }
        }
    }
}