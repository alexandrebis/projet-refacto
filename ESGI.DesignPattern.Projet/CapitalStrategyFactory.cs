using System;

namespace ESGI.DesignPattern.Projet
{
    public abstract class CapitalStrategyFactory
    {
        public static CapitalStrategy Create(CapitalStrategyType csType)
        {
            switch(csType)
            {
                case CapitalStrategyType.Term: return new CapitalStrategyTermLoan();
                case CapitalStrategyType.Revolver: return new CapitalStrategyRevolver();
                case CapitalStrategyType.AdvisedLine: return new CapitalStrategyAdvisedLine();
                default: throw new System.Exception();
            }
        }
    }
}