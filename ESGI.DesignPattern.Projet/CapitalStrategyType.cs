using System;

namespace ESGI.DesignPattern.Projet
{
    public enum CapitalStrategyType
    {
        Term,
        Revolver,
        AdvisedLine
    }
}